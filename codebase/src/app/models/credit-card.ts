export interface CreditCard {
  id: number;
  name: string;
  number: string;
  cvv: number;
  expiredDate: string;
  type: string;
  limit: number;
  isDefault: boolean;
}
