export interface Receipt {
  id: number;
  date: string;
  totalPrice: number;
  // products: [Product];
}
