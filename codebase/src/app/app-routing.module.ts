import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './base-content/page-not-found/page-not-found.component';
import { PaymentComponent } from './payment/payment.component';

// https://www.samjulien.com/add-routing-existing-angular-project
const routes: Routes = [
  // TODO: Redirect for development.
  { path: '', redirectTo: 'payment', pathMatch: 'full'},
  { path: 'payment', component: PaymentComponent },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
