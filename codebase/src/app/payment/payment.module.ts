import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentComponent } from './payment.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { CardInputComponent } from './card-input/card-input.component';
import { AddressInputComponent } from './address-input/address-input.component';
import { AddressInfoComponent } from './address-info/address-info.component';
import { CardInfoComponent } from './card-info/card-info.component';

@NgModule({
  declarations: [
    PaymentComponent,
    CheckoutComponent,
    CardInputComponent,
    AddressInputComponent,
    AddressInfoComponent,
    CardInfoComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    PaymentComponent,
    AddressInputComponent
  ]
})
export class PaymentModule { }
