import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class PaymentService {

  private REST_API_SERVER = 'http://localhost:3000';

  constructor(private httpClient: HttpClient) { }

  getContacts(): any {
    return this.httpClient.get(this.REST_API_SERVER + '/posts');
  }

  // getContacts(): any {
  //   const contactList = [
  //     {ContactId: 1, ContactName: 'James'},
  //     {ContactId: 2, ContactName: 'Clark'},
  //     {ContactId: 3, ContactName: 'Ruby'},
  //     {ContactId: 4, ContactName: 'Jack'}
  //   ];
  //   return contactList;
  // }
  //
  // callingFromTemplate(): void {
  //   console.log('calling from template');
  // }

}
