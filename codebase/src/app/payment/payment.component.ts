import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PaymentService } from './payment.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  constructor(public paymentService: PaymentService) { }
  // constructor(private router: Router) { }

  // tslint:disable-next-line:variable-name
  contacts = [];

  ngOnInit(): void {
    this.paymentService
      .getContacts()
      .subscribe(data => {
        this.contacts = data;
      });
  }

  // goPayment(): void {
  //   this.router.navigate(['payment']);
  // }

}
